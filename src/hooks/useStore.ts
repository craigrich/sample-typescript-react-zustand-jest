import create from 'zustand';
import { createGridSlice, GridState } from 'features/Mars/stores/gridSlice';
import { createRobotSlice, RobotState } from 'features/Mars/stores/robotSlice';

export type AppState = GridState & RobotState;

export const useStore = create<AppState>((set, get) => ({
  ...createGridSlice(set, get),
  ...createRobotSlice(set, get),
}));
