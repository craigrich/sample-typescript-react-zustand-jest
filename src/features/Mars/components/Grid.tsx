import { HStack, Stack } from '@chakra-ui/layout';
import { useStore } from 'hooks/useStore';
import { GridItem } from './GridItem';

export const Grid = () => {
  const { maxLat, maxLng } = useStore();

  return (
    <Stack spacing="8" align="center">
      {[...new Array(maxLng + 1)]
        .map((_, lng) => {
          return (
            <HStack key={lng} spacing="8">
              {[...new Array(maxLat + 1)].map((_, lat) => (
                <GridItem key={`${lat}${lng}`} lat={lat} lng={lng} />
              ))}
            </HStack>
          );
        })
        .reverse()}
    </Stack>
  );
};
