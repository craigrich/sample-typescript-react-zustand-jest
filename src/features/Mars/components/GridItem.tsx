import { Avatar, AvatarGroup } from '@chakra-ui/avatar';
import { Box, Text } from '@chakra-ui/layout';
import { useStore } from 'hooks/useStore';
import { useMemo } from 'react';
import { Marker } from './Marker';

type GridItemProps = {
  lat: number;
  lng: number;
};

export const GridItem = ({ lat, lng }: GridItemProps) => {
  const { robots } = useStore();
  const activeRobots = useMemo(
    () => robots.filter((robot) => robot.lat === lat && robot.lng === lng),
    [lat, lng, robots]
  );

  return (
    <Box
      key={`${lat + lng}`}
      background="rgba(0,0,0,0.3)"
      border="1px solid white"
      position="relative"
      height="80px"
      width="80px"
    >
      <AvatarGroup size="sm" max={2}>
        {activeRobots.map((robot) => (
          <Avatar
            key={robot.id}
            icon={<Marker robot={robot} />}
            bg={robot.lost ? 'red' : 'gray.300'}
          />
        ))}
      </AvatarGroup>
      <Text
        fontSize="sm"
        color="white"
        position="absolute"
        bottom={1}
        right={1}
      >
        <span>{lat}</span>
        <span>{lng}</span>
      </Text>
    </Box>
  );
};
