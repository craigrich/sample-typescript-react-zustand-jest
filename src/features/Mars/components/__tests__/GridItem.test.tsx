import { render, screen } from '@testing-library/react';
import { useStore } from 'hooks/useStore';
import { Robot } from 'types';
import { GridItem } from '../GridItem';

describe('GridItem', () => {
  const renderComponent = (partialRobot: Partial<Robot> = {}) => {
    const robot = {
      id: 12345,
      lat: 2,
      lng: 3,
      instruction: '',
      orientation: 'N',
    } as Robot;
    useStore.setState({ robots: [robot] });
    return render(<GridItem lat={2} lng={3} />);
  };

  it('should display a given latitude', () => {
    renderComponent();
    expect(screen.getByText(2)).toBeInTheDocument();
  });
  it('should display a given longitude', () => {
    renderComponent();
    expect(screen.getByText(3)).toBeInTheDocument();
  });
  it('should display any robots that match the items latitude & longitude', () => {
    renderComponent();

    expect(screen.getByText(12345)).toBeInTheDocument();
  });
});
