import { render, screen } from '@testing-library/react';
import { Robot } from 'types';
import { Marker } from '../Marker';

describe('Marker', () => {
  const renderComponent = (partialRobot: Partial<Robot> = {}) => {
    const robot = {
      id: 1,
      lat: 0,
      lng: 0,
      instruction: '',
      orientation: 'N',
      ...partialRobot,
    } as Robot;
    return render(<Marker robot={robot} />);
  };

  it('should display a given robot id', () => {
    renderComponent({ id: 2 });
    expect(screen.getByText(2)).toBeInTheDocument();
  });

  it.each`
    orientation | arrow
    ${'N'}      | ${'↑'}
    ${'S'}      | ${'↓'}
    ${'E'}      | ${'→'}
    ${'W'}      | ${'←'}
  `(
    'should display $arrow if the robots orientation is $orientation',
    ({ orientation, arrow }) => {
      renderComponent({ orientation });
      expect(screen.getByText(arrow)).toBeInTheDocument();
    }
  );
});
