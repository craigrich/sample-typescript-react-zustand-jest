import { Flex } from '@chakra-ui/layout';
import marsImg from '../assets/mars.jpg';
import { Grid } from './Grid';

export const Mars = () => {
  return (
    <Flex
      p={40}
      flex="1"
      justifyContent="center"
      flexDirection="column"
      backgroundImage={marsImg}
      backgroundSize="cover"
    >
      <Grid />
    </Flex>
  );
};
