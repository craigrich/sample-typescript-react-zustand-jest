import { Orientation, Robot } from 'types';

const getArrow = (orientation: Orientation): string => {
  switch (orientation) {
    case 'N':
      return '↑';
    case 'S':
      return '↓';
    case 'E':
      return '→';
    case 'W':
      return '←';
    default:
      return '';
  }
};

export type MarkerProps = {
  robot: Robot;
};

export const Marker = ({ robot }: MarkerProps) => {
  return (
    <div>
      <span>{getArrow(robot.orientation)}</span>
      <span>{robot.id}</span>
    </div>
  );
};
