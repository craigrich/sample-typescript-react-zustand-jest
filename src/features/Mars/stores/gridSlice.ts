import { GetState, SetState } from 'zustand';
import { AppState } from 'hooks/useStore';
import { Robot } from 'types';

export type GridState = {
  maxLat: number;
  maxLng: number;
  setGrid: (maxLat: number, maxLng: number) => void;
};

export const createGridSlice = (
  set: SetState<AppState>,
  get: GetState<AppState>
): GridState => ({
  maxLat: 5,
  maxLng: 3,
  setGrid: (maxLat, maxLng) => set({ maxLat, maxLng }),
});

export const isOutOfBounds = (state: AppState, robot: Robot): boolean =>
  robot.lat > state.maxLat || robot.lng > state.maxLng;
