import { GetState, SetState } from 'zustand';
import { Command, Robot } from 'types';
import { AppState } from 'hooks/useStore';
import { isOutOfBounds } from './gridSlice';
import { sleep } from '../utils/sleep';
import { moveRobot } from '../utils/moveRobot';

export type RobotState = {
  robots: Robot[];
  setRobots: (robots: Robot[]) => void;
  setRobot: (robot: Robot) => void;
  startRobots: () => void;
  processCommand: (robotId: Robot['id'], command: Command) => void;
};

export const createRobotSlice = (
  set: SetState<AppState>,
  get: GetState<AppState>
): RobotState => ({
  robots: [],
  setRobots: (robots) => set({ robots: [...robots] }),
  setRobot: (updatedRobot) =>
    set(({ robots }) => ({
      robots: robots.map((robot) =>
        updatedRobot.id === robot.id ? updatedRobot : robot
      ),
    })),
  startRobots: async () => {
    const { robots, processCommand } = get();
    for (const robot of robots) {
      for (const command of [...robot.instruction]) {
        await processCommand(robot.id, command as Command);
      }
    }
  },
  processCommand: async (robotId, command) => {
    await sleep(100);
    const state = get();
    const { setRobot } = state;

    const robotState = getRobotById(state, robotId);
    const nextRobotState = moveRobot(robotState, command);

    if (nextRobotState instanceof Error) {
      console.warn(nextRobotState);
      return;
    }

    if (
      isOutOfBounds(state, nextRobotState) &&
      isForbiddenZone(state, robotState)
    ) {
      return;
    }

    if (
      isOutOfBounds(state, nextRobotState) &&
      !isForbiddenZone(state, robotState)
    ) {
      setRobot({ ...robotState, lost: true });
      return;
    }

    setRobot(nextRobotState);
  },
});

export const isForbiddenZone = (state: AppState, robot: Robot): boolean =>
  !!state.robots.find(
    ({ lost, lat, lng }) => lost && lat === robot.lat && lng === robot.lng
  );

export const getRobotById = (state: AppState, robotId: number): Robot => {
  return state.robots.find(({ id }) => id === robotId) as Robot;
};
