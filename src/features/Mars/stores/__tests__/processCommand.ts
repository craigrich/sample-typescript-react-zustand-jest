import { useStore } from 'hooks/useStore';
import { Robot } from 'types';

const mockRobotId = 123;
const mockRobot = {
  id: mockRobotId,
  lat: 3,
  lng: 3,
  instruction: 'FRFRF',
  orientation: 'N',
  lost: false,
} as Robot;

describe('processCommand', () => {
  const store = useStore;
  const { processCommand } = store.getState();
  const getRobot = () => store.getState().robots[0];
  const setRobot = (partialRobot: Partial<Robot>) => {
    const robot = { ...mockRobot, ...partialRobot };
    store.setState({ robots: [robot] });
  };

  beforeEach(() => {
    store.setState({
      maxLat: 5,
      maxLng: 6,
      robots: [],
    });
  });

  it('should halt command if robot is lost', async () => {
    setRobot({ lost: true, lat: 3, lng: 4 });
    await processCommand(mockRobotId, 'F');

    const robot = getRobot();
    expect(robot.lat).toBe(3);
    expect(robot.lng).toBe(4);
  });

  it('should halt command if on a forbidden zone and next move is out of bounds', async () => {
    setRobot({ lat: 4, lng: 6 });
    await processCommand(mockRobotId, 'F');

    const robot = getRobot();
    expect(robot.lat).toBe(4);
    expect(robot.lng).toBe(6);
  });

  it('should mark a robot as lost if next move is out of bounds', async () => {
    setRobot({ lat: 3, lng: 6 });
    await processCommand(mockRobotId, 'F');

    const robot = getRobot();
    expect(robot.lat).toBe(3);
    expect(robot.lng).toBe(6);
    expect(robot.lost).toBe(true);
  });

  it('should move the robot', async () => {
    setRobot({ lat: 3, lng: 4 });
    await processCommand(mockRobotId, 'F');

    const robot = getRobot();
    expect(robot.lat).toBe(3);
    expect(robot.lng).toBe(5);
  });

  it.each`
    axis     | orientation | value | expected
    ${'lng'} | ${'N'}      | ${3}  | ${4}
    ${'lng'} | ${'S'}      | ${3}  | ${2}
    ${'lat'} | ${'E'}      | ${2}  | ${3}
    ${'lat'} | ${'W'}      | ${2}  | ${1}
  `(
    `should move to $axis $expected when $axis is $value and facing $orientation`,
    async ({ axis, orientation, value, expected }) => {
      setRobot({ [axis]: value, orientation });
      await processCommand(mockRobotId, 'F');

      const robot = getRobot();
      expect(robot[axis]).toBe(expected);
    }
  );

  it.each`
    command | orientation | expected
    ${'R'}  | ${'N'}      | ${'E'}
    ${'R'}  | ${'S'}      | ${'W'}
    ${'R'}  | ${'E'}      | ${'S'}
    ${'R'}  | ${'W'}      | ${'N'}
    ${'L'}  | ${'N'}      | ${'W'}
    ${'L'}  | ${'S'}      | ${'E'}
    ${'L'}  | ${'E'}      | ${'N'}
    ${'L'}  | ${'W'}      | ${'S'}
  `(
    'should rotate to $expected when facing $orientation and command is $command',
    async ({ command, orientation, expected }) => {
      setRobot({ orientation });
      await processCommand(mockRobotId, command);

      const robot = getRobot();
      expect(robot.orientation).toBe(expected);
    }
  );
});
