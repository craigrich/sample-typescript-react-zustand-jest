import { Robot } from 'types';
import create from 'zustand/vanilla';
import { createRobotSlice, getRobotById } from '../robotSlice';

const robotId = 123;
const robot = {
  id: robotId,
  lat: 4,
  lng: 5,
  instruction: 'F',
  orientation: 'N',
} as Robot;

describe('robotSlice', () => {
  it('should set a list of robots', () => {
    const store = create(createRobotSlice);
    const { setRobots } = store.getState();
    setRobots([robot]);

    const { robots } = store.getState();
    expect(robots.length).toBe(1);
    expect(robots[0].lat).toBe(4);
    expect(robots[0].lng).toBe(5);
  });

  it('should set a single robot', () => {
    const store = create(createRobotSlice);
    const { setRobots, setRobot } = store.getState();
    setRobots([robot]);
    setRobot({ ...robot, lat: 6, lng: 7 });

    const { robots } = store.getState();
    expect(robots.length).toBe(1);
    expect(robots[0].lat).toBe(6);
    expect(robots[0].lng).toBe(7);
  });

  describe('startRobots', () => {
    const processCommand = jest.fn();
    const store = create(createRobotSlice);
    store.setState({
      robots: [
        { ...robot, id: 123, instruction: 'FR' },
        { ...robot, id: 456, instruction: 'LFR' },
      ],
      processCommand,
    });

    it('should iterate over each robot and process its list of commands', async () => {
      const { startRobots } = store.getState();
      await startRobots();
      expect(processCommand.mock.calls).toEqual([
        [123, 'F'],
        [123, 'R'],
        [456, 'L'],
        [456, 'F'],
        [456, 'R'],
      ]);
    });
  });

  describe('getRobotById', () => {
    const store = create(createRobotSlice);
    const robotId = 123;
    const robot = {
      id: robotId,
      lat: 4,
      lng: 5,
      instruction: 'F',
      orientation: 'N',
    } as Robot;

    beforeEach(() => {
      const { setRobots } = store.getState();
      setRobots([robot]);
    });

    it('should return a robot given a valid robot ID', () => {
      const state = store.getState();
      const result = getRobotById(state, robotId);
      expect(result).toEqual(robot);
    });

    it('should return nothing a robot is not found', () => {
      const state = store.getState();
      const result = getRobotById(state, robotId + 123);
      expect(result).toBe(undefined);
    });
  });
});
