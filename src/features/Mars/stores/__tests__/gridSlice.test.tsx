import { useStore } from 'hooks/useStore';
import { isOutOfBounds } from '../gridSlice';

describe('gridSlice', () => {
  const store = useStore;

  it('should set a grid', () => {
    const { setGrid } = store.getState();
    setGrid(6, 7);

    const { maxLat, maxLng } = store.getState();
    expect(maxLat).toBe(6);
    expect(maxLng).toBe(7);
  });

  describe('isOutOfBounds', () => {
    beforeEach(() => {
      const { setGrid } = store.getState();
      setGrid(6, 7);
    });

    it('should return true if a robots lat is greater than maxLat', () => {
      const state = store.getState();
      const result = isOutOfBounds(state, { id: 123, lat: 4, lng: 8 });
      expect(result).toBe(true);
    });

    it('should return false if a robots lat is equal or less than maxLat', () => {
      const state = store.getState();
      const result = isOutOfBounds(state, { id: 123, lat: 4, lng: 5 });
      expect(result).toBe(false);
    });

    it('should return true if a robots lng is greater than maxLng', () => {
      const state = store.getState();
      const result = isOutOfBounds(state, { id: 123, lat: 4, lng: 8 });
      expect(result).toBe(true);
    });

    it('should return false if a robots lat is equal or less than maxLng', () => {
      const state = store.getState();
      const result = isOutOfBounds(state, { id: 123, lat: 4, lng: 8 });
      expect(result).toBe(true);
    });
  });
});
