import { Command, Robot } from 'types';
import { moveRobot } from '../moveRobot';

describe('moveRobot', () => {
  const returnMoveRobot = (partialRobot: Partial<Robot>, command: Command) => {
    const robot = {
      id: 123,
      ...partialRobot,
    } as Robot;
    return moveRobot(robot, command);
  };

  describe('moveForward', () => {
    it.each`
      axis     | orientation | value | expected
      ${'lng'} | ${'N'}      | ${3}  | ${4}
      ${'lng'} | ${'S'}      | ${3}  | ${2}
      ${'lat'} | ${'E'}      | ${2}  | ${3}
      ${'lat'} | ${'W'}      | ${2}  | ${1}
    `(
      'set $axis to $expected when $axis is $value and orientation is $orientation',
      ({ axis, value, orientation, expected }) => {
        const nextRobotState = returnMoveRobot(
          { [axis]: value, orientation },
          'F'
        );
        expect(nextRobotState[axis]).toBe(expected);
      }
    );
  });

  describe('rotateRight', () => {
    it.each`
      value  | expected
      ${'N'} | ${'E'}
      ${'S'} | ${'W'}
      ${'E'} | ${'S'}
      ${'W'} | ${'N'}
    `(
      'set orientation to $expected when orientation is $value',
      ({ value, expected }) => {
        const nextRobotState = returnMoveRobot({ orientation: value }, 'R');
        expect(nextRobotState.orientation).toBe(expected);
      }
    );
  });

  describe('rotateLeft', () => {
    it.each`
      value  | expected
      ${'N'} | ${'W'}
      ${'S'} | ${'E'}
      ${'E'} | ${'N'}
      ${'W'} | ${'S'}
    `(
      'set orientation to $expected when orientation is $value',
      ({ value, expected }) => {
        const nextRobotState = returnMoveRobot({ orientation: value }, 'L');
        expect(nextRobotState.orientation).toBe(expected);
      }
    );
  });
});
