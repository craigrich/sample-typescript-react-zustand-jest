import { Command, Robot } from 'types';

export class InvalidOrientationError extends TypeError {}
export class InvalidCommandError extends TypeError {}
export class InvalidRobotError extends ReferenceError {}

const rotateLeft = (robot: Robot): Robot | InvalidOrientationError => {
  switch (robot.orientation) {
    case 'N':
      return { ...robot, orientation: 'W' };
    case 'S':
      return { ...robot, orientation: 'E' };
    case 'E':
      return { ...robot, orientation: 'N' };
    case 'W':
      return { ...robot, orientation: 'S' };
    default:
      return new InvalidOrientationError(
        `Invalid Orientation: ${robot.orientation}`
      );
  }
};

const rotateRight = (robot: Robot): Robot | InvalidOrientationError => {
  switch (robot.orientation) {
    case 'N':
      return { ...robot, orientation: 'E' };
    case 'S':
      return { ...robot, orientation: 'W' };
    case 'E':
      return { ...robot, orientation: 'S' };
    case 'W':
      return { ...robot, orientation: 'N' };
    default:
      return new InvalidOrientationError(
        `Invalid Orientation: ${robot.orientation}`
      );
  }
};

const moveForward = (robot: Robot): Robot | InvalidOrientationError => {
  switch (robot.orientation) {
    case 'N':
      return { ...robot, lng: robot.lng + 1 };
    case 'S':
      return { ...robot, lng: robot.lng - 1 };
    case 'E':
      return { ...robot, lat: robot.lat + 1 };
    case 'W':
      return { ...robot, lat: robot.lat - 1 };
    default:
      return new InvalidOrientationError(
        `Invalid Orientation: ${robot.orientation}`
      );
  }
};

export const moveRobot = (
  robot: Robot,
  command: Command
):
  | Robot
  | InvalidRobotError
  | InvalidOrientationError
  | InvalidCommandError => {
  if (robot.lost) {
    return new InvalidRobotError(`Invalid Robot: Robot is lost :(`);
  }
  switch (command) {
    case 'L':
      return rotateLeft(robot);
    case 'R':
      return rotateRight(robot);
    case 'F':
      return moveForward(robot);
    default:
      return new InvalidCommandError(`Invalid Command: ${command}`);
  }
};
