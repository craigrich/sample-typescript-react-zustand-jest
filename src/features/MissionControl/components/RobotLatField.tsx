import { NumberField } from 'components/Form';
import { useController, useWatch } from 'react-hook-form';

type RobotLatFieldProps = {
  name: string;
};

export const RobotLatField = ({ name }: RobotLatFieldProps) => {
  const maxLat = useWatch({ name: 'maxLat' });
  const { field, fieldState } = useController({
    name,
    defaultValue: 0,
    rules: {
      validate: (v) => v <= maxLat,
    },
  });
  return (
    <NumberField
      isInvalid={fieldState.invalid}
      errorMessage="Invalid Lat"
      max={maxLat}
      onChange={field.onChange}
      value={field.value}
    />
  );
};
