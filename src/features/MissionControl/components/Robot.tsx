import { Heading, HStack, Stack } from '@chakra-ui/layout';
import { RobotLatField } from './RobotLatField';
import { RobotLngField } from './RobotLngField';
import { RobotOrientationField } from './RobotOrientationField';
import { RobotInstructionField } from './RobotInstructionField';

type RobotProps = {
  robotNumber: number;
};

export const Robot = ({ robotNumber }: RobotProps) => {
  return (
    <Stack spacing={4}>
      <Heading size="sm">Robot {robotNumber + 1}</Heading>
      <HStack spacing={4}>
        <RobotLatField name={`robots.${robotNumber}.lat`} />
        <RobotLngField name={`robots.${robotNumber}.lng`} />
        <RobotOrientationField name={`robots.${robotNumber}.orientation`} />
      </HStack>
      <RobotInstructionField name={`robots.${robotNumber}.instruction`} />
    </Stack>
  );
};
