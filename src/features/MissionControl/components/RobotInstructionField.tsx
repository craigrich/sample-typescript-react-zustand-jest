import { InputField } from 'components/Form/InputField';
import { useController } from 'react-hook-form';
import { Command } from 'types';

type RobotInstructionFieldProps = {
  name: string;
};

export const isValidInstructions = (
  instructions: string,
  validCommands: Command[] = ['L', 'R', 'F']
): boolean => {
  return [...instructions.trim().toUpperCase()].every((char) =>
    validCommands.includes(char as Command)
  );
};

export const RobotInstructionField = ({ name }: RobotInstructionFieldProps) => {
  const { field, fieldState } = useController({
    defaultValue: '',
    rules: { validate: isValidInstructions },
    name,
  });
  return (
    <InputField
      placeholder="Instruction. e.g. LLFFFLFLFL"
      onChange={field.onChange}
      value={field.value}
      isInvalid={fieldState.invalid}
      errorMessage="Invalid Instruction"
    />
  );
};
