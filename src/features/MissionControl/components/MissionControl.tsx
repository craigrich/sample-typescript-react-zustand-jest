import { Button, Stack } from '@chakra-ui/react';
import { FormProvider, SubmitHandler, useForm } from 'react-hook-form';
import { WorldCoords } from './WorldCoords';
import { Robots } from './Robots';

import { useStore } from 'hooks/useStore';
import { Console } from './Console';
import { Robot } from 'types';

type FormFields = {
  maxLat: number;
  maxLng: number;
  robots: Robot[];
};

export const MissionControl = () => {
  const { maxLat, maxLng, setGrid, setRobots, startRobots } = useStore();
  const formMethods = useForm<FormFields>({
    defaultValues: {
      maxLat,
      maxLng,
      robots: [
        {
          id: 1,
          lat: 1,
          lng: 1,
          orientation: 'E',
          instruction: 'RFRFRFRF',
        },
        {
          id: 2,
          lat: 3,
          lng: 2,
          orientation: 'N',
          instruction: 'FRRFLLFFRRFLL',
        },
        {
          id: 3,
          lat: 0,
          lng: 3,
          orientation: 'W',
          instruction: 'LLFFFLFLFL',
        },
      ],
    },
  });

  const onSubmit: SubmitHandler<FormFields> = (data) => {
    setGrid(data.maxLat, data.maxLng);
    setRobots(data.robots);
    startRobots();
  };

  return (
    <FormProvider {...formMethods}>
      <form onSubmit={formMethods.handleSubmit(onSubmit)}>
        <Stack spacing={4}>
          <WorldCoords />
          <Robots />
          <Button size="lg" w="full" colorScheme="green" type="submit">
            Execute
          </Button>
        </Stack>
      </form>
      <Console />
    </FormProvider>
  );
};
