import { Heading, HStack, Stack } from '@chakra-ui/layout';
import { NumberField } from 'components/Form';
import { useController } from 'react-hook-form';

export const WorldCoords = () => {
  const { field: latField } = useController({ name: 'maxLat' });
  const { field: lngField } = useController({ name: 'maxLng' });
  return (
    <Stack mt={3}>
      <Heading size="sm" mb="1">
        World coordinates
      </Heading>
      <HStack spacing={8}>
        <NumberField
          min={1}
          max={50}
          label="latitude"
          onChange={latField.onChange}
          value={latField.value}
        />
        <NumberField
          min={1}
          max={50}
          label="Longitude"
          onChange={lngField.onChange}
          value={lngField.value}
        />
      </HStack>
    </Stack>
  );
};
