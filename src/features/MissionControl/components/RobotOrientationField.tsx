import { SelectField } from 'components/Form';
import { useController } from 'react-hook-form';

type RobotOrientationFieldProps = {
  name: string;
};

export const RobotOrientationField = ({ name }: RobotOrientationFieldProps) => {
  const { field } = useController({ name, defaultValue: 'W' });
  return (
    <SelectField
      options={['N', 'S', 'E', 'W']}
      onChange={field.onChange}
      value={field.value}
    />
  );
};
