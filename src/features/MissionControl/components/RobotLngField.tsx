import { NumberField } from 'components/Form';
import { useController, useWatch } from 'react-hook-form';

type RobotLngFieldProps = {
  name: string;
};

export const RobotLngField = ({ name }: RobotLngFieldProps) => {
  const maxLng = useWatch({ name: 'maxLng' });
  const { field, fieldState } = useController({
    name,
    defaultValue: 0,
    rules: {
      validate: (v) => v <= maxLng,
    },
  });
  return (
    <NumberField
      isInvalid={fieldState.invalid}
      errorMessage="Invalid Long"
      max={maxLng}
      onChange={field.onChange}
      value={field.value}
    />
  );
};
