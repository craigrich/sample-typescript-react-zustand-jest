import { Box, Divider, Heading, Text } from '@chakra-ui/react';
import { useStore } from 'hooks/useStore';

export const Console = () => {
  const { robots } = useStore();

  return (
    <Box
      bg="white"
      width={300}
      height={200}
      position="fixed"
      bottom={3}
      right={3}
      p={4}
    >
      <Heading fontSize="md">Output</Heading>
      <Divider my={2} />
      {robots.map((robot) => (
        <Text key={robot.id}>
          {robot.lat} {robot.lng} {robot.orientation} {robot.lost && 'LOST'}
        </Text>
      ))}
    </Box>
  );
};
