import { Button } from '@chakra-ui/button';
import { Divider, HStack, StackDivider, VStack } from '@chakra-ui/layout';
import { useFieldArray } from 'react-hook-form';
import { Robot } from './Robot';

export const Robots = () => {
  const { fields, append, remove } = useFieldArray({
    name: 'robots',
    keyName: 'robotId',
  });
  return (
    <>
      <VStack spacing={4} divider={<StackDivider />}>
        {fields.map((field, index) => (
          <Robot key={field.robotId} robotNumber={index} />
        ))}
      </VStack>
      <Divider my={6} />
      <HStack justify="flex-end" mt={4}>
        {fields.length && (
          <Button colorScheme="red" onClick={() => remove(fields.length - 1)}>
            Delete
          </Button>
        )}
        <Button
          colorScheme="teal"
          onClick={() => append({ id: fields.length + 1 })}
        >
          Add
        </Button>
      </HStack>
    </>
  );
};
