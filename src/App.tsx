import { Flex } from '@chakra-ui/layout';
import { Sidebar } from 'components/Sidebar';
import { MissionControl } from './features/MissionControl';
import { Mars } from './features/Mars';

function App() {
  return (
    <Flex justifyContent="space-between">
      <Sidebar>
        <MissionControl />
      </Sidebar>
      <Mars />
    </Flex>
  );
}

export default App;
