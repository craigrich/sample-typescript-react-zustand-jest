import { Select } from '@chakra-ui/select';

export type SelectFieldProps = {
  onChange: (value: string) => void;
  options: string[];
  value: string;
};

export const SelectField = ({ onChange, options, value }: SelectFieldProps) => {
  return (
    <Select
      width={100}
      required
      value={value}
      onChange={(selectEvent) => onChange(selectEvent.target.value)}
    >
      {options.map((option) => (
        <option key={option} value={option}>
          {option}
        </option>
      ))}
    </Select>
  );
};
