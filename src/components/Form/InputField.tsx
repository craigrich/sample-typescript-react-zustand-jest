import { FormControl, FormErrorMessage } from '@chakra-ui/form-control';
import { Input } from '@chakra-ui/input';

export type InputFieldProps = {
  placeholder: string;
  isInvalid: boolean;
  onChange: (value: string) => void;
  value: string;
  errorMessage: string;
};

export const InputField = ({
  placeholder,
  isInvalid,
  onChange,
  value,
  errorMessage,
}: InputFieldProps) => {
  return (
    <FormControl isInvalid={isInvalid}>
      <Input
        placeholder={placeholder}
        onChange={(inputEvent) => onChange(inputEvent.target.value)}
        value={value}
        max={100}
        required
      />
      <FormErrorMessage>{errorMessage}</FormErrorMessage>
    </FormControl>
  );
};
