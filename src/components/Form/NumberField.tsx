import {
  FormControl,
  FormErrorMessage,
  FormLabel,
} from '@chakra-ui/form-control';
import {
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from '@chakra-ui/number-input';

export type NumberFieldProps = {
  label?: string;
  value: number;
  onChange: (value: number) => void;
  max?: number;
  min?: number;
  isInvalid?: boolean;
  errorMessage?: string;
};

export const NumberField = ({
  label,
  onChange,
  value,
  max = 50,
  min = 0,
  isInvalid,
  errorMessage,
}: NumberFieldProps) => {
  return (
    <FormControl width={100} isInvalid={isInvalid}>
      {label && <FormLabel>{label}</FormLabel>}
      <NumberInput
        onChange={(_, valueAsNumber) => onChange(valueAsNumber)}
        value={value}
        max={max}
        min={min}
        required
      >
        <NumberInputField />
        <NumberInputStepper>
          <NumberIncrementStepper />
          <NumberDecrementStepper />
        </NumberInputStepper>
      </NumberInput>
      <FormErrorMessage>{errorMessage}</FormErrorMessage>
    </FormControl>
  );
};
