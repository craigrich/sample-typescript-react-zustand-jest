import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SelectField, SelectFieldProps } from '../SelectField';

describe('SelectField', () => {
  const renderComponent = (partialProps: Partial<SelectFieldProps> = {}) => {
    const props = {
      onChange: () => {},
      options: [],
      value: '',
      ...partialProps,
    };
    return render(<SelectField {...props} />);
  };

  it('should display a list of given options', () => {
    renderComponent({ options: ['Option One', 'Option Two'] });
    expect(screen.getByText('Option One')).toBeInTheDocument();
    expect(screen.getByText('Option Two')).toBeInTheDocument();
  });
  it('should display a given value', () => {
    renderComponent({
      value: 'Option Two',
      options: ['Option One', 'Option Two'],
    });
    expect(screen.getByDisplayValue('Option Two')).toBeInTheDocument();
  });
  it('should call onChange from user input', () => {
    const onChange = jest.fn();
    renderComponent({
      value: 'Option Two',
      options: ['Option One', 'Option Two'],
      onChange,
    });
    userEvent.selectOptions(
      screen.getByDisplayValue('Option Two'),
      'Option One'
    );
    expect(onChange).toBeCalledWith('Option One');
  });
});
