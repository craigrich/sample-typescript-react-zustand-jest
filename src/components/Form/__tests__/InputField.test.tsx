import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { InputFieldProps, InputField } from '../InputField';

describe('InputField', () => {
  const renderComponent = (partialProps: Partial<InputFieldProps> = {}) => {
    const props = {
      placeholder: '',
      value: '',
      errorMessage: '',
      isInvalid: false,
      onChange: () => {},
      ...partialProps,
    };
    return render(<InputField {...props} />);
  };

  it('should display a given placeholder', () => {
    renderComponent({ placeholder: 'Example Placeholder' });
    expect(
      screen.getByPlaceholderText('Example Placeholder')
    ).toBeInTheDocument();
  });
  it('should display a given value', () => {
    renderComponent({ value: 'Example Value' });
    expect(screen.getByDisplayValue('Example Value')).toBeInTheDocument();
  });
  it('should call onChange from user input', () => {
    const onChange = jest.fn();
    renderComponent({ onChange, value: 'Example Value' });
    userEvent.click(screen.getByDisplayValue('Example Value'));
    userEvent.keyboard('a');
    expect(onChange).toBeCalledWith('Example Valuea');
  });
  it('should show an error message if the field is invalid', () => {
    renderComponent({ errorMessage: 'Example Error', isInvalid: true });
    expect(screen.getByText('Example Error')).toBeInTheDocument();
  });
});
