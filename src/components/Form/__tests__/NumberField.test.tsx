import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { NumberField, NumberFieldProps } from '../NumberField';

describe('NumberField', () => {
  const renderComponent = (partialProps: Partial<NumberFieldProps> = {}) => {
    const props = {
      label: '',
      value: 1,
      onChange: () => {},
      isInvalid: false,
      errorMessage: '',
      ...partialProps,
    };
    return render(<NumberField {...props} />);
  };

  it('should display a given label', () => {
    renderComponent({ label: 'Example Label' });
    expect(screen.getByText('Example Label')).toBeInTheDocument();
  });
  it('should display a given value', () => {
    renderComponent({ value: 2 });
    expect(screen.getByDisplayValue(2)).toBeInTheDocument();
  });
  it('should call onChange from user input', () => {
    const onChange = jest.fn();
    renderComponent({ onChange, value: 2 });
    userEvent.click(screen.getByDisplayValue(2));
    userEvent.keyboard('3');
    expect(onChange).toBeCalledWith(23);
  });
  it('should show an error message if the field is invalid', () => {
    renderComponent({ errorMessage: 'Example Error', isInvalid: true });
    expect(screen.getByText('Example Error')).toBeInTheDocument();
  });
});
