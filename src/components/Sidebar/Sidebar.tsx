import { ReactNode } from 'react';
import { Divider, Flex, Link, Stack, Text } from '@chakra-ui/layout';
import { FaLinkedinIn, FaGithub } from 'react-icons/fa';

type SidebarProps = {
  children: ReactNode;
};

export const Sidebar = ({ children }: SidebarProps) => {
  return (
    <Stack
      borderRight="1px"
      borderRightColor="gray.200"
      minH="100vh"
      px="8"
      py="4"
    >
      <Flex>
        <Link
          href="https://github.com/craigrich/martian-robots"
          target="_blank"
          mr="2"
        >
          <FaGithub size="1.2em" />
        </Link>
        <Link
          href="https://www.linkedin.com/in/craigalanrichardson/"
          target="_blank"
        >
          <FaLinkedinIn size="1.2em" />
        </Link>
      </Flex>
      <Divider my={8} />
      <Text py={2} fontSize="3xl" fontFamily="monospace" fontWeight="bold">
        🤖 Martian Robots 🤖
      </Text>
      <Divider my={8} />
      {children}
    </Stack>
  );
};
