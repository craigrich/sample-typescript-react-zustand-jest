import { screen, render } from '@testing-library/react';
import { Sidebar } from '../Sidebar';

describe('Sidebar', () => {
  beforeEach(() => {
    render(
      <Sidebar>
        <div>Example Content</div>
      </Sidebar>
    );
  });
  it('should display the app title', () => {
    expect(screen.getByText('🤖 Martian Robots 🤖')).toBeInTheDocument();
  });
  it('should display its given children', () => {
    expect(screen.getByText('Example Content')).toBeInTheDocument();
  });
});
