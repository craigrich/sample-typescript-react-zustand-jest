export type Command = 'L' | 'R' | 'F';

export type Orientation = 'N' | 'S' | 'E' | 'W';

export type Robot = {
  id: number;
  lat: number;
  lng: number;
  instruction: string;
  orientation: Orientation;
  lost?: boolean;
};
