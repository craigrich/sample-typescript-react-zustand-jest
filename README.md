# 🤖 Martian Robots 🤖

This is the repo showcases my approach to building React applications. Feel free to explore the codebase and visit the app here:

[Martian Robots Demo](https://laughing-golick-845b5a.netlify.app)

## Get Started

Prerequisites:

- Node 14 +

To set up the app execute the following commands.

```bash
git clone https://github.com/craigrich/martian-robots
cd martian-robots
npm install
```

## Available Scripts

For convenience, I bootstrapped this project with Create React App. It contains all the related npm scripts.

#### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### `npm test`

Launches the test runner in the interactive watch mode.

#### `npm build`

Builds the app for production to the `build` folder.
